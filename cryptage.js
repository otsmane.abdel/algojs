let chaine = 'admin';

let crypTable = [];

for (let index = 0; index < chaine.length; index ++){
    
   
    if(chaine[index] == 'a' ){
        crypTable = crypTable + 'b';
        

    } else if(chaine[index] == 'd'){
        crypTable = crypTable + 'e';

    } else if(chaine[index] == 'm'){
        crypTable = crypTable + 'n';
       
       
    } else if(chaine[index] == 'i'){
        crypTable = crypTable + 'j';
        
        
    } else if(chaine[index] == 'n'){
        crypTable = crypTable + 'o';

       
    }

}
console.log(`La chaine d'origine: ${chaine} \n`);
console.log(`La chaine cryptée: ${crypTable} \n`);

let uncrypTable = [];

for (let index = 0; index < crypTable.length; index ++){
    
   
    if(crypTable[index] == 'b'){
        uncrypTable = uncrypTable + 'a';
        

    } else if(crypTable[index] == 'e'){
        uncrypTable = uncrypTable + 'd';

    } else if(crypTable[index] == 'n'){
        uncrypTable = uncrypTable + 'm';
       
       
    } else if(crypTable[index] == 'j'){
        uncrypTable = uncrypTable + 'i';
        
        
    } else if(crypTable[index] == 'o'){
        uncrypTable = uncrypTable + 'n';

       
    }

}
console.log(`La chaine décryptée: ${uncrypTable} \n`);



function cryptageLettre (chaineEntree){
    let crypTab = [];

    for (let index = 0; index < chaineEntree.length; index ++){
   
        if(chaineEntree[index] == 'a' ){
            crypTab = crypTab + 'b';

        } else if(chaineEntree[index] == 'b'){
            crypTab = crypTab + 'c';
    
        } else if(chaineEntree[index] == 'c'){
            crypTab = crypTab + 'd';
              
        } else if(chaineEntree[index] == 'd'){
            crypTab = crypTab + 'e';
              
        }else if(chaineEntree[index] == 'e'){
            crypTab = crypTab + 'f';
                
        } else if(chaineEntree[index] == 'f'){
            crypTab = crypTab + 'g';    
        }else if(chaineEntree[index] == 'g'){
            crypTab = crypTab + 'h';
    
        } else if(chaineEntree[index] == 'h'){
            crypTab = crypTab + 'i';
              
        } else if(chaineEntree[index] == 'i'){
            crypTab = crypTab + 'j';
                
        } else if(chaineEntree[index] == 'j'){
            crypTab = crypTab + 'k';    
        }else if(chaineEntree[index] == 'k'){
            crypTab = crypTab + 'l';
    
        } else if(chaineEntree[index] == 'l'){
            crypTab = crypTab + 'm';
              
        } else if(chaineEntree[index] == 'm'){
            crypTab = crypTab + 'n';
                
        } else if(chaineEntree[index] == 'n'){
            crypTab = crypTab + 'o';  

        }else if(chaineEntree[index] == 'o'){
            crypTab = crypTab + 'p';
    
        } else if(chaineEntree[index] == 'p'){
            crypTab = crypTab + 'q';
              
        } else if(chaineEntree[index] == 'q'){
            crypTab = crypTab + 'r';
                
        } else if(chaineEntree[index] == 'r'){
            crypTab = crypTab + 's';    
        }
        else if(chaineEntree[index] == 's'){
            crypTab = crypTab + 't';
    
        } else if(chaineEntree[index] == 't'){
            crypTab = crypTab + 'u';
              
        } else if(chaineEntree[index] == 'u'){
            crypTab = crypTab + 'v';
                
        } else if(chaineEntree[index] == 'v'){
            crypTab = crypTab + 'w'; 

        } else if(chaineEntree[index] == 'w'){
            crypTab = crypTab + 'x';
              
        } else if(chaineEntree[index] == 'x'){
            crypTab = crypTab + 'y';
                
        } else if(chaineEntree[index] == 'y'){
            crypTab = crypTab + 'z';    
        }
        else if(chaineEntree[index] == 'z'){
            crypTab = crypTab + 'a';    
        }
    
    }
        console.log(`la chaine cryptée est: ${crypTab}`);
        return crypTab;
       

}

let exemple = 'abracadabra';

let exempleCrypte = cryptageLettre(exemple);


function decryptageLettre (chaineCryp){
    let decrypTab = [];

    for (let index = 0; index < chaineCryp.length; index ++){
   
        if(chaineCryp[index] == 'a' ){
            decrypTab = decrypTab + 'z';

        } else if(chaineCryp[index] == 'b'){
            decrypTab = decrypTab + 'a';
    
        } else if(chaineCryp[index] == 'c'){
            decrypTab = decrypTab + 'b';
              
        } else if(chaineCryp[index] == 'd'){
            decrypTab = decrypTab + 'c';
                
        } else if(chaineCryp[index] == 'e'){
            decrypTab = decrypTab + 'd'; 

        }else if(chaineCryp[index] == 'f'){
            decrypTab = decrypTab + 'e';
    
        } else if(chaineCryp[index] == 'g'){
            decrypTab = decrypTab + 'f';
              
        } else if(chaineCryp[index] == 'h'){
            decrypTab = decrypTab + 'g';
                
        } else if(chaineCryp[index] == 'i'){
            decrypTab = decrypTab + 'h';   

        }else if(chaineCryp[index] == 'j'){
            decrypTab = decrypTab + 'i';
    
        } else if(chaineCryp[index] == 'k'){
            decrypTab = decrypTab + 'j';
              
        } else if(chaineCryp[index] == 'l'){
            decrypTab = decrypTab + 'k';
                
        } else if(chaineCryp[index] == 'm'){
            decrypTab = decrypTab + 'l';  

        }else if(chaineCryp[index] == 'n'){
            decrypTab = decrypTab + 'm';
    
        } else if(chaineCryp[index] == 'o'){
            decrypTab = decrypTab + 'n';
              
        } else if(chaineCryp[index] == 'p'){
            decrypTab = decrypTab + 'o';
                
        } else if(chaineCryp[index] == 'q'){
            decrypTab = decrypTab + 'p';    
        }
        else if(chaineCryp[index] == 'r'){
            decrypTab = decrypTab + 'q';
    
        } else if(chaineCryp[index] == 's'){
            decrypTab = decrypTab + 'r';
              
        } else if(chaineCryp[index] == 't'){
            decrypTab = decrypTab + 's';
                
        } else if(chaineCryp[index] == 'u'){
            decrypTab = decrypTab + 't'; 

        } else if(chaineCryp[index] == 'v'){
            decrypTab = decrypTab + 'u';
              
        } else if(chaineCryp[index] == 'w'){
            decrypTab = decrypTab + 'v';
                
        } else if(chaineCryp[index] == 'x'){
            decrypTab = decrypTab + 'w';    
        }
        else if(chaineCryp[index] == 'y'){
            decrypTab = decrypTab + 'x';    
        }
    
    }
    console.log(`la chaine décrypté est: ${decrypTab}`);
    return decrypTab;

    

}

let exempleCrypteDecrypt = decryptageLettre(exempleCrypte);

/**Tester un truc plus propre
 * avec moins de codes!
 */



