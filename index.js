/**
 * main
 * @author Abdel <adressemail@mail.fr>
 * Approche algoritmique
 *  -définition des variable
 *  -affectation de valeurs aux variables
 *  -les différents  types scalaires
 * @version 1.0.0
 */

let myName = '';
let yourName = '';

//affectation de la Aubert à la variable myName
myName = 'Aubert';
yourName = 'Bond';
//affichage du contenue de la variable myName
console.log(myName); //Expected output : Aubert


//Changer la valeur de myName par autre chose

myName ='Autre chose';
console.log(myName);



let isMale = true;

let users = [
    'Aubert',
    'Abdel',
    'Nesibe',
    'Rainui',
    'Remy'
];

//affecter le contenu d'une variable à une autre variable

myName = yourName;
yourName = 'Superman';
console.log(`${myName} <=> ${yourName}`);
console.log(users);

for (let index = 0;  index <= 4; index =index + 1){
    console.log(users[index]);
}

let indice = 0;
while (indice < users.length){
    console.log(users[indice]);
    indice = indice + 1;

}

//index vaut 5

let index = 0;

do {
    console.log(users[index]);
    index = index + 1;

}while (index < users.length)

if (users[0] == 'Aubert') {
console.log(`Salut ${users[0]}`);
}

if (users[3] == 'Aubert') {
    console.log(`Hola ${users[3]}`);
    }else{
        console.log(`T'es pas Aubert, du coup Salut ${users[3]}`);
    }