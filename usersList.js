class User {
    constructor() {
        this.name = '';
        this.fistName = '';
        this.id = '';
        this.password = '';
    }
}

/**
 * Fonction unique qui a en paramètres 
 * unique (user: User, list: User[])
 * @param user: User Une variable de type User
 * @param list: User[] un tableau de User
 * @returns boolean
 */
function unique(user, list){
    let yesYouCan = true;
    for (let index = 0; index < list.length; index = index + 1){
        if (list[index].id === user.id){
            yesYouCan = false;
        }
    }
    return yesYouCan;
}


/**
 * 
 * @param {*} user some user variable
 * @param {*} list A list of users
 * @return User[]
 */
function ajouter (user, list) {
    if (unique(user, list)){
        list.push(user);
    }
    return list;
}

/**
 * Début de l'application Administrateur
 */

let listUsers = [];

const user = new User();
user.name = 'Aubert';
user.fistName = 'Jean-luc';
user.id = 'jlaubert';
user.password = 'admin';

const abdel = new User();
abdel.name = 'Abdel';
abdel.fistName = 'Abdel';
abdel.id = 'abdel';
abdel.password = 'admin';

listUsers = ajouter (user, listUsers);




// Pseudo test: expect listUser got 1 item
console.log(`Items : ${listUsers.length}`);
console.log(`L'ID de l'utilisateur ${listUsers.length} est ${user.id} `);



listUsers = ajouter (abdel, listUsers);
// Pseudo test: expect listUser got 2 items
console.log(`Items : ${listUsers.length}`);
console.log(`L'ID de l'utilisateur ${listUsers.length} est ${abdel.id} `);

abdel.id = 'jlaubert';


console.log(`L'ID de l'utilisateur ${listUsers.length} est ${abdel.id} `);

listUsers.forEach((user) => {
    console.log (`User: ${user.name} : ${user.id}`);
    
});


